import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import 'package:test_mobile/model/home_page_info.dart';
import 'package:test_mobile/widgets/apply_info.dart';
import 'package:test_mobile/widgets/cv_background_info.dart';
import 'package:test_mobile/widgets/cv_info.dart';
import 'package:test_mobile/widgets/header.dart';
import 'package:test_mobile/widgets/nav_bar.dart';
import 'package:test_mobile/widgets/toggle_button_bar.dart';

class WebScreen extends StatefulWidget {
  const WebScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<WebScreen> createState() => _WebScreenState();
}

class _WebScreenState extends State<WebScreen> {
  final _scrollController = ScrollController();
  bool hasPassed = false;
  final buttonKey = GlobalKey();
  double buttonPosition = 0;
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset > buttonPosition) {
        setState(() {
          hasPassed = true;
        });
      } else {
        setState(() {
          hasPassed = false;
        });
      }
    });
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      final RenderObject? box = buttonKey.currentContext?.findRenderObject();
      buttonPosition = (box as RenderBox).localToGlobal(Offset.zero).dy;
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            controller: _scrollController,
            child: Column(
              children: [
                Header(hasPassed: hasPassed, buttonKey: buttonKey),
                SizedBox(height: 40.h),
                ToggleButtonBar(
                  onChange: (index) {
                    setState(() {
                      _currentIndex = index;
                    });
                  },
                ),
                SizedBox(height: 55.h),
                Text(
                  'Drei einfache Schritte\nzu deinem neuen Job',
                  style: TextStyle(
                    fontSize: 40.sp,
                    color: const Color(0xff4A5568),
                  ),
                ),
                SizedBox(height: 135.h),
                Stack(
                  clipBehavior: Clip.antiAlias,
                  children: [
                    Column(
                      children: [
                        CvInfoWidget(
                          info: homePageInfo[_currentIndex][0],
                        ),
                        SizedBox(height: 200.h),
                        CvBackgroundInfoWidget(
                          info: homePageInfo[_currentIndex][1],
                        ),
                        SizedBox(height: 200.h),
                        ApplyInfoWidget(
                          info: homePageInfo[_currentIndex][2],
                        ),
                        SizedBox(height: 230.h),
                      ],
                    ),
                    Positioned(
                      top: 120.h,
                      left: 446.w,
                      child: Container(
                        height: 580.h,
                        width: 580.h,
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            image: Svg('assets/images/right_arrow.svg'),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 625.h,
                      left: 600.w,
                      child: Container(
                        height: 600.h,
                        width: 530.w,
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            image: Svg('assets/images/left_arrow.svg'),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          NavBar(hasPassed: hasPassed),
          const NavBarBorder(),
        ],
      ),
    );
  }
}
