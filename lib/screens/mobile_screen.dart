import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_mobile/model/home_page_info.dart';
import 'package:test_mobile/widgets/apply_info.dart';
import 'package:test_mobile/widgets/cv_background_info.dart';
import 'package:test_mobile/widgets/cv_info.dart';
import 'package:test_mobile/widgets/header.dart';
import 'package:test_mobile/widgets/nav_bar.dart';
import 'package:test_mobile/widgets/toggle_button_bar.dart';

class MobileScreen extends StatefulWidget {
  const MobileScreen({Key? key}) : super(key: key);

  @override
  State<MobileScreen> createState() => _MobileScreenState();
}

class _MobileScreenState extends State<MobileScreen> {
  int _currentIndex = 0;

  final List<String> titles = [
    'Drei einfache Schritte\nzu deinem neuen Job',
    'Drei einfache Schritte\nzu deinem neuen Mitarbeiter',
    'Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            SizedBox(height: 2000.h),
            const Header(
              hasPassed: false,
            ),
            Positioned(
              top: 556.h,
              left: 0,
              right: 0,
              child: Container(
                padding: EdgeInsets.all(20.w),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(16.0),
                    topRight: Radius.circular(16.0),
                  ),
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0, -1),
                      blurRadius: 3,
                      color: Color(0x33000000),
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 24.h,
                    ),
                    const CallToActionButton(),
                    SizedBox(
                      height: 44.h,
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 710.h,
              left: 0,
              right: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  SizedBox(
                    height: 27.h,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: ToggleButtonBar(
                        onChange: (index) {
                          setState(() {
                            _currentIndex = index;
                          });
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30.h,
                  ),
                  Center(
                    child: Text(
                      titles[_currentIndex],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 21.sp,
                        color: const Color(0xff4A5568),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              top: 865.h,
              left: 0,
              right: 0,
              child: CvInfoWidget(
                info: homePageInfo[_currentIndex][0],
              ),
            ),
            Positioned(
              top: 1430.h,
              left: 0,
              right: 0,
              child: ApplyInfoWidget(
                info: homePageInfo[_currentIndex][2],
              ),
            ),
            Positioned(
              top: 1108.h,
              left: 0,
              right: 0,
              child: CvBackgroundInfoWidget(
                info: homePageInfo[_currentIndex][1],
              ),
            ),
            const NavBar(hasPassed: false),
            const NavBarBorder(),
          ],
        ),
      ),
    );
  }
}
