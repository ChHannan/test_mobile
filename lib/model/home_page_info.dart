class HomePageInfo {
  final String title;
  final String image;

  HomePageInfo({
    required this.title,
    required this.image,
  });
}

final List<List<HomePageInfo>> homePageInfo = [
  [
    HomePageInfo(
      title: 'Erstellen dein Lebenslauf',
      image: 'assets/images/profile.svg',
    ),
    HomePageInfo(
      title: 'Erstellen dein Lebenslauf',
      image: 'assets/images/task.svg',
    ),
    HomePageInfo(
      title: 'Mit nur einem Klick\nbewerben',
      image: 'assets/images/personal_file.svg',
    ),
  ],
  [
    HomePageInfo(
      title: 'Erstellen dein\nUnternehmensprofil',
      image: 'assets/images/profile.svg',
    ),
    HomePageInfo(
      title: 'Erstellen ein Jobinserat',
      image: 'assets/images/about_me.svg',
    ),
    HomePageInfo(
      title: 'Wähle deinen neuen\nMitarbeiter aus',
      image: 'assets/images/swipe_profiles.svg',
    ),
  ],
  [
    HomePageInfo(
      title: 'Erstellen dein\nUnternehmensprofil',
      image: 'assets/images/profile.svg',
    ),
    HomePageInfo(
      title: 'Erhalte Vermittlungs-\nangebot von Arbeitgeber',
      image: 'assets/images/job_offers.svg',
    ),
    HomePageInfo(
      title: 'Vermittlung nach\nProvision oder\nStundenlohn',
      image: 'assets/images/business_deal.svg',
    ),
  ]
];

