import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import 'package:test_mobile/model/home_page_info.dart';
import 'package:test_mobile/widgets/adaptive_page_builder.dart';

class ApplyInfoWidget extends StatelessWidget {
  final HomePageInfo info;

  const ApplyInfoWidget({
    Key? key,
    required this.info,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final titles = info.title.split('\n');

    return AdaptivePageBuilder(
      builder: (context, deviceType, _, __) {
        return deviceType == DeviceTypeEnum.desktop
            ? Row(
                children: [
                  SizedBox(width: 465.w),
                  SizedBox(
                    width: 560.w,
                    child: Stack(
                      children: [
                        Container(
                          height: 305.r,
                          width: 305.r,
                          decoration: BoxDecoration(
                            color: const Color(0xffF7FAFC).withOpacity(0.5),
                            borderRadius: BorderRadius.circular(155.r),
                          ),
                        ),
                        Positioned(
                          left: 100.w,
                          child: Row(
                            children: [
                              Text(
                                '3.',
                                style: TextStyle(
                                  fontSize: 130.sp,
                                  color: const Color(0xff718096),
                                  fontFamily: 'Lato',
                                ),
                              ),
                              SizedBox(width: 25.w),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 36.h,
                                  ),
                                  Text(
                                    titles[0],
                                    style: TextStyle(
                                        fontSize: 30.sp,
                                        color: const Color(0xff718096),
                                        letterSpacing: 0.9,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: 'Lato'),
                                  ),
                                  for (String i in titles.sublist(1))
                                    Text(
                                      i,
                                      style: TextStyle(
                                          fontSize: 30.sp,
                                          color: const Color(0xff718096),
                                          letterSpacing: 0.9,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: 'Lato'),
                                    ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 65.w),
                  Container(
                    height: 275.h,
                    width: 500.w,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: Svg(info.image),
                      ),
                    ),
                  )
                ],
              )
            : SizedBox(
                height: 327.h,
                child: Stack(
                  children: [
                    Positioned(
                      left: -54.w,
                      child: Container(
                        height: 305.r,
                        width: 305.r,
                        decoration: BoxDecoration(
                          color: const Color(0xffF7FAFC).withOpacity(0.5),
                          borderRadius: BorderRadius.circular(155.r),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 56.w,
                      child: Row(
                        children: [
                          Text(
                            '3.',
                            style: TextStyle(
                              fontSize: 130.sp,
                              color: const Color(0xff718096),
                              fontFamily: 'Lato',
                            ),
                          ),
                          SizedBox(width: 25.w),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                titles[0],
                                style: TextStyle(
                                  fontSize: 16.sp,
                                  color: const Color(0xff718096),
                                  letterSpacing: 0.47,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: 'Lato',
                                ),
                              ),
                              for (String i in titles.sublist(1))
                                Text(
                                  i,
                                  style: TextStyle(
                                    fontSize: 16.sp,
                                    color: const Color(0xff718096),
                                    letterSpacing: 0.47,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: 'Lato',
                                  ),
                                ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Positioned(
                      top: 173.h,
                      left: 60.w,
                      child: Container(
                        height: 197.h,
                        width: 250.w,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: Svg(info.image),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
      },
    );
  }
}
