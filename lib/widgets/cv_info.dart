import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import 'package:test_mobile/model/home_page_info.dart';
import 'package:test_mobile/widgets/adaptive_page_builder.dart';

class CvInfoWidget extends StatelessWidget {
  final HomePageInfo info;

  const CvInfoWidget({
    Key? key,
    required this.info,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final titles = info.title.split('\n');
    return AdaptivePageBuilder(
      builder: (context, deviceType,_, __) {
        return deviceType == DeviceTypeEnum.desktop
            ? Row(
                children: [
                  SizedBox(width: 300.w),
                  SizedBox(
                    width: 560.w,
                    child: Stack(
                      children: [
                        Container(
                          height: 210.r,
                          width: 210.r,
                          decoration: BoxDecoration(
                            color: const Color(0xffF7FAFC).withOpacity(0.5),
                            borderRadius: BorderRadius.circular(100.r),
                          ),
                        ),
                        Positioned(
                          top: 29.h,
                          left: 46.w,
                          child: RichText(
                            text: TextSpan(
                              text: '1.',
                              style: TextStyle(
                                fontSize: 130.sp,
                                color: const Color(0xff718096),
                                fontFamily: 'Lato',
                              ),
                              children: [
                                WidgetSpan(
                                  alignment: PlaceholderAlignment.baseline,
                                  baseline: TextBaseline.alphabetic,
                                  child: SizedBox(width: 20.w),
                                ),
                                TextSpan(
                                  text: info.title,
                                  style: TextStyle(
                                    fontSize: 30.sp,
                                    color: const Color(0xff718096),
                                    letterSpacing: 0.9,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: 'Lato',
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 65.w),
                  Container(
                    height: 253.h,
                    width: 384.w,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: Svg(info.image),
                      ),
                    ),
                  ),
                ],
              )
            : SizedBox(
                height: 265.h,
                child: Stack(
                  children: [
                    Positioned(
                      top: 80.h,
                      left: -34.w,
                      child: Container(
                        height: 210.r,
                        width: 210.r,
                        decoration: BoxDecoration(
                          color: const Color(0xffF7FAFC).withOpacity(0.5),
                          borderRadius: BorderRadius.circular(100.r),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 85.h,
                      left: 12.w,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            '1.',
                            style: TextStyle(
                                fontSize: 130.sp,
                                color: const Color(0xff718096),
                                fontFamily: 'Lato'),
                          ),
                          SizedBox(width: 25.w),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                titles[0],
                                style: TextStyle(
                                    fontSize: 16.sp,
                                    color: const Color(0xff718096),
                                    letterSpacing: 0.47,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: 'Lato'),
                              ),
                              for (String i in titles.sublist(1))
                                Text(
                                  i,
                                  style: TextStyle(
                                    fontSize: 16.sp,
                                    color: const Color(0xff718096),
                                    letterSpacing: 0.47,
                                    fontWeight: FontWeight.w400,
                                    fontFamily: 'Lato',
                                  ),
                                ),
                              SizedBox(height: 25.h),
                            ],
                          )
                        ],
                      ),
                    ),
                    Positioned(
                      left: 100.w,
                      child: Container(
                        height: 145.h,
                        width: 220.w,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: Svg(info.image),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
      },
    );
  }
}
