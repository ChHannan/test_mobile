import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_mobile/utils/on_hover.dart';

class NavBar extends StatelessWidget {
  const NavBar({
    Key? key,
    required this.hasPassed,
  }) : super(key: key);

  final bool hasPassed;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: kToolbarHeight,
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(16.0),
          bottomRight: Radius.circular(16.0),
        ),
        boxShadow: [
          BoxShadow(
            color: Color(0x29000000),
            blurRadius: 6.0,
            offset: Offset(0.0, 3.0),
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          if (hasPassed)
            Row(
              children: [
                Text(
                  'Jetzt Klicken',
                  style: TextStyle(
                    fontSize: 20.sp,
                    fontWeight: FontWeight.w500,
                    color: const Color(0xff4A5568),
                  ),
                ),
                SizedBox(width: 20.w),
                OutlinedButton(
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                    ),
                    minimumSize: MaterialStateProperty.all(Size(255.w, 40.h)),
                    backgroundColor: MaterialStateProperty.resolveWith<Color?>(
                          (Set<MaterialState> states) {
                        if (states.contains(MaterialState.hovered)) {
                          return const Color(0xffE6FFFA);
                        }
                        return null;
                      },
                    ),
                  ),
                  onPressed: () {},
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Text(
                      'Kostenlos Registrieren',
                      style: TextStyle(
                        color: const Color(0xff319795),
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.84,
                        fontSize: 14.sp,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 20.w),
              ],
            ),
          OnHover(
            builder: (isHovered) => Text(
              'Login',
              style: TextStyle(
                fontSize: 14.sp,
                decoration:
                isHovered ? TextDecoration.underline : TextDecoration.none,
                color: const Color(0xff319795),
                fontWeight: FontWeight.w600,
              ),
            ),
          )
        ],
      ),
    );
  }
}


class NavBarBorder extends StatelessWidget {
  const NavBarBorder({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 4,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Color(0xff319795),
            Color(0xff3182CE),
          ],
        ),
      ),
    );
  }
}
