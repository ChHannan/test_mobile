import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import 'package:test_mobile/widgets/adaptive_page_builder.dart';

class Header extends StatelessWidget {
  const Header({
    Key? key,
    required this.hasPassed,
    this.buttonKey,
  }) : super(key: key);

  final bool hasPassed;
  final GlobalKey<State<StatefulWidget>>? buttonKey;

  @override
  Widget build(BuildContext context) {
    return AdaptivePageBuilder(
      builder: (context, deviceType, _, __) {
        return deviceType == DeviceTypeEnum.desktop
            ? ClipPath(
                clipper: LandingClipper(),
                child: Container(
                  height: 600.h,
                  width: double.maxFinite,
                  margin: const EdgeInsets.only(top: kToolbarHeight),
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color(0xffEBF4FF),
                        Color(0xffE6FFFA),
                      ],
                    ),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 375.w,
                      ),
                      SizedBox(
                        width: 320.w,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Deine Job\nwebsite',
                              style: TextStyle(
                                fontSize: 65.sp,
                                letterSpacing: 1.95,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            SizedBox(height: 52.h),
                            if (!hasPassed)
                              CallToActionButton(buttonKey: buttonKey),
                          ],
                        ),
                      ),
                      SizedBox(width: 150.w),
                      Container(
                        height: 455.r,
                        width: 455.r,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(225.r),
                          image: const DecorationImage(
                            image: Svg('assets/images/agreement.svg'),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            : Container(
                height: 600.h,
                width: double.maxFinite,
                margin: const EdgeInsets.only(top: kToolbarHeight),
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color(0xffEBF4FF),
                      Color(0xffE6FFFA),
                    ],
                  ),
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 18.h,
                    ),
                    Text(
                      'Deine Job\nwebsite',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 42.sp,
                        letterSpacing: 1.26,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(height: 2.h),
                    Image(
                      height: 405.h,
                      width: 418.w,
                      image: const Svg('assets/images/agreement.svg'),
                    ),
                  ],
                ),
              );
      },
    );
  }
}

class CallToActionButton extends StatelessWidget {
  const CallToActionButton({
    Key? key,
    this.buttonKey,
  }) : super(key: key);

  final GlobalKey<State<StatefulWidget>>? buttonKey;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      key: buttonKey,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        gradient: const LinearGradient(
          colors: [
            Color(0xff319795),
            Color(0xff3182CE),
          ],
        ),
      ),
      child: TextButton(
        style: ButtonStyle(
          minimumSize: MaterialStateProperty.all(
            Size.fromHeight(40.h),
          ),
          backgroundColor: MaterialStateProperty.resolveWith<Color?>(
            (Set<MaterialState> states) {
              if (states.contains(MaterialState.disabled)) {
                return const Color(0xff319795);
              }
              return null; // Use the component's default.
            },
          ),
        ),
        onPressed: () {},
        child: const Padding(
          padding: EdgeInsets.symmetric(vertical: 12),
          child: Text(
            'Kostenlos Registrieren',
            style: TextStyle(
                color: Color(0xffE6FFFA),
                fontWeight: FontWeight.w600,
                letterSpacing: 0.84),
          ),
        ),
      ),
    );
  }
}

class LandingClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    double height = size.height;
    double width = size.width;

    var path = Path();
    path.lineTo(0, height);
    path.cubicTo(
        width / 2, height, width - 70.h, height - 70.h, width, height - 50.h);
    path.lineTo(width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
