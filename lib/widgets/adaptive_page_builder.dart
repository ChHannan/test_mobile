import 'dart:ui';

import 'package:flutter/material.dart';

enum DeviceTypeEnum {
  mobile,
  desktop,
}

typedef Width = double;
typedef Height = double;

/// Builder Function
typedef AdaptiveWidgetBuilder = Widget Function(
    BuildContext, DeviceTypeEnum, Width, Height);

class AdaptivePageBuilder extends StatefulWidget {
  final AdaptiveWidgetBuilder builder;

  const AdaptivePageBuilder({
    required this.builder,
    Key? key,
  }) : super(key: key);

  @override
  State<AdaptivePageBuilder> createState() => _AdaptivePageBuilderState();
}

class _AdaptivePageBuilderState extends State<AdaptivePageBuilder>
    with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();

    /// Registers the observer
    WidgetsBinding.instance?.addObserver(this);

    /// Schedules a first `didChangeMetrics` to decide on the device type
    WidgetsBinding.instance?.addPostFrameCallback((_) => didChangeMetrics());

    /// Attempts to get initial device type
    currentType = _getDeviceType;
    width = _width ?? window.physicalSize.width;
    height = _height ?? window.physicalSize.height;
  }

  late DeviceTypeEnum currentType;
  late double width;
  late double height;

  /// Calculates the device-independent width
  double? get _width => WidgetsBinding.instance != null
      ? WidgetsBinding.instance!.window.physicalSize.width /
          WidgetsBinding.instance!.window.devicePixelRatio
      : null;

  /// Calculates the device-independent height
  double? get _height => WidgetsBinding.instance != null
      ? WidgetsBinding.instance!.window.physicalSize.height /
      WidgetsBinding.instance!.window.devicePixelRatio
      : null;

  /// Decides which UI we should show depending on the width of the screen
  DeviceTypeEnum get _getDeviceType {
    if ((_width ?? 0) < 768) {
      return DeviceTypeEnum.mobile;
    }
    return DeviceTypeEnum.desktop;
  }

  /// Calculate the new width of the screen and, if necessary, change
  /// the current device type
  @override
  void didChangeMetrics() {
    final newType = _getDeviceType;

    if (newType != currentType) {
      currentType = newType;
      setState(() {});
    }
  }

  /// Builds the children with the given BuildContext and the DeviceTypeEnum
  @override
  Widget build(BuildContext context) {
    return widget.builder(context, currentType, width, height);
  }
}
