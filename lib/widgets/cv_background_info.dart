import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import 'package:test_mobile/model/home_page_info.dart';
import 'package:test_mobile/widgets/adaptive_page_builder.dart';

class CvBackgroundInfoWidget extends StatelessWidget {
  final HomePageInfo info;

  const CvBackgroundInfoWidget({
    Key? key,
    required this.info,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final titles = info.title.split('\n');
    return AdaptivePageBuilder(
      builder: (context, deviceType, _, __) {
        return deviceType == DeviceTypeEnum.desktop
            ? ClipPath(
                clipper: WebBackgroundClipper(),
                child: Container(
                  height: 370.h,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color(0xffE6FFFA),
                        Color(0xffEBF4FF),
                      ],
                    ),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SizedBox(width: 555.w),
                      Container(
                        height: 275.h,
                        width: 325.w,
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            image: Svg('assets/images/task.svg'),
                          ),
                        ),
                      ),
                      SizedBox(width: 120.w),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          RichText(
                            text: TextSpan(
                              text: '2. ',
                              style: TextStyle(
                                  fontSize: 130.sp,
                                  color: const Color(0xff718096),
                                  fontFamily: 'Lato'),
                              children: [
                                TextSpan(
                                  text: info.title,
                                  style: TextStyle(
                                      fontSize: 30.sp,
                                      color: const Color(0xff718096),
                                      letterSpacing: 0.9,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: 'Lato'),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 85.h),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            : ClipPath(
                clipper: MobileBackgroundClipper(),
                child: Container(
                  height: 370.h,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color(0xffE6FFFA),
                        Color(0xffEBF4FF),
                      ],
                    ),
                  ),
                  child: Stack(
                    children: [
                      Positioned(
                        top: 180.h,
                        left: 75.w,
                        child: Container(
                          height: 140.h,
                          width: 230.w,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: Svg(info.image),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        top: 26.h,
                        left: 37.w,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              '2.',
                              style: TextStyle(
                                  fontSize: 130.sp,
                                  color: const Color(0xff718096),
                                  fontFamily: 'Lato'),
                            ),
                            SizedBox(width: 25.w),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  titles[0],
                                  style: TextStyle(
                                      fontSize: 16.sp,
                                      color: const Color(0xff718096),
                                      letterSpacing: 0.47,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: 'Lato'),
                                ),
                                for (String i in titles.sublist(1))
                                  Text(
                                    i,
                                    style: TextStyle(
                                        fontSize: 16.sp,
                                        color: const Color(0xff718096),
                                        letterSpacing: 0.47,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: 'Lato'),
                                  ),
                                SizedBox(height: 25.h),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
      },
    );
  }
}

class WebBackgroundClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    double height = size.height;
    double width = size.width;

    Path path = Path();
    path.moveTo(width * 0.34, height * 0.07);
    path.cubicTo(width * 0.2, height * -0.04, width * 0.05, height * 0.002, 0,
        size.height * 0.04);
    path.lineTo(0, height * 0.99);
    path.cubicTo(width * 0.06, height * 0.96, width * 0.094, height * 0.83,
        width * 0.41, height * 0.99);
    path.cubicTo(width * 0.72, height * 0.98, width * 0.91, height * 0.79,
        width, height * 0.69);
    path.lineTo(width, height * 0.23);
    path.cubicTo(width * 0.87, height * -0.21, width * 0.54, height * 0.20,
        width * 0.34, height * 0.07);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}

class MobileBackgroundClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    double height = size.height;
    double width = size.width;

    Path path = Path();
    path.moveTo(width * 0.37, height * 0.085);
    path.cubicTo(width * 0.25, height * 0.072, width * 0.15, height * -0.018, 0,
        height * 0.003);
    path.lineTo(0, height * 0.998);
    path.cubicTo(width * 0.082, height * 0.97, width * 0.24, height * 0.9065934,
        width * 0.5308123, height * 0.9536209);
    path.cubicTo(width * 0.76, height * 0.99, width * 0.94, height * 0.91,
        width, height * 0.88);
    path.lineTo(width, height * 0.024);
    path.cubicTo(width * 0.92, height * -0.015, width * 0.76, height * 0.024,
        width * 0.69, height * 0.049);
    path.cubicTo(width * 0.62, height * 0.07, width * 0.47, height * 0.11,
        width * 0.37, height * 0.086);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
