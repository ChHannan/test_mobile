import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ToggleButtonBar extends StatefulWidget {
  final Function(int) onChange;

  const ToggleButtonBar({
    Key? key,
    required this.onChange,
  }) : super(key: key);

  @override
  State<ToggleButtonBar> createState() => _ToggleButtonBarState();
}

class _ToggleButtonBarState extends State<ToggleButtonBar> {
  final List<bool> _selections = List.generate(
    3,
    (int index) => index == 0 ? true : false,
  );
  final keys = [GlobalKey(), GlobalKey(), GlobalKey()];

  @override
  Widget build(BuildContext context) {
    return ToggleButtons(
      color: const Color(0xff319795),
      borderRadius: BorderRadius.circular(12.0),
      borderColor: const Color(0xffCBD5E0),
      constraints: const BoxConstraints(minWidth: 160, minHeight: 40),
      isSelected: _selections,
      fillColor: const Color(0xff81E6D9),
      selectedColor: const Color(0xffE6FFFA),
      textStyle: TextStyle(fontSize: 14.sp, letterSpacing: 0.84),
      onPressed: (int index) {
        setState(() {
          for (int i = 0; i < _selections.length; i++) {
            _selections[i] = i == index;
          }
          widget.onChange(index);
          Scrollable.ensureVisible(
            keys[index].currentContext!,
            alignment: 0.3,
          );
        });
      },
      children: <Widget>[
        Text(
          'Arbeitnehmer',
          key: keys[0],
        ),
        Text(
          'Arbeitgeber',
          key: keys[1],
        ),
        Text(
          'Temporärbüro',
          key: keys[2],
        ),
      ],
    );
  }
}
