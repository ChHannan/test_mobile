import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_mobile/screens/mobile_screen.dart';
import 'package:test_mobile/screens/web_screen.dart';
import 'package:test_mobile/widgets/adaptive_page_builder.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AdaptivePageBuilder(builder: (context, deviceType, width, height) {
      Size designSize = const Size(1920, 1080);
      if (width <= 564) {
        designSize = const Size(360, 640);
      } else if (width <= 768) {
        designSize = const Size(768, 1280);
      } else if (width <= 1024) {
        designSize = const Size(1024, 1920);
      }
      return ScreenUtilInit(
        designSize: designSize,
        builder: (context, child) {
          return MaterialApp(
            title: 'Test Mobile',
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              fontFamily: 'Lato',
              scaffoldBackgroundColor: Colors.white,
            ),
            home: child,
          );
        },
        child: const HomeScreen(),
      );
    });
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AdaptivePageBuilder(
      builder: (context, deviceType, _, __) =>
          deviceType == DeviceTypeEnum.desktop
              ? const WebScreen()
              : const MobileScreen(),
    );
  }
}
